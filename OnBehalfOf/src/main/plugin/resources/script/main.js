/*
 * $Revision$
 * $Date$
 *
 * Copyright (C) 1999-2011 Jive Software. All rights reserved.
 *
 * This software is the proprietary information of Jive Software. Use is subject to license terms.
 */
jive.namespace('custom.discussions');

/**
 * @depends path=/resources/scripts/jive/namespace.js
 * @depends path=/resources/scripts/lib/jiverscripts/src/oo/class.js
 * @depends path=/resources/scripts/apps/content/discussions/main.js
 *
 */
jive.custom.discussions.Main = jive.discussions.Main.extend(function(protect, _super) {

    this.init = function(options) {
        // Use a custom web service when saving data
        this.options = $j.extend({
            resourceType: 'customDiscussion'
        }, options);

        // Perform default behavior
        _super.init.call(this, this.options);

        // Render user picker for OnBehalfOf field
        var autocomplete = new jive.UserPicker.Main({
            multiple: false,
            valueIsUsername: false,
            emailAllowed: false,
            listAllowed: true,
            canInvitePartners: true,
            $input: options.$onBehalfOf,
            onBehalfOf: true
        });
    };

    this.getObjectId = function() {
        return this.options.actionBean.threadID;
    };

});

