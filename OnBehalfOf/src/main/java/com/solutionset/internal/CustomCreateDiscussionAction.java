package com.solutionset.internal;

import com.jivesoftware.community.content.discussion.action.CreateDiscussionAction;
import com.jivesoftware.community.util.JiveContainerPermHelper;

import java.util.Map;

/**
 * Customized to add additional boolean model parameter "isGlobalAdmin".
 * <br /> <br />
 *
 * {@link JiveContainerPermHelper#isGlobalAdmin()}
 */
public class CustomCreateDiscussionAction extends CreateDiscussionAction {

    @Override
    public String input() {
        String result = super.input();

        Map<String, Object> model = (Map<String, Object>) this.model;
        model.put("isGlobalAdmin", JiveContainerPermHelper.isGlobalAdmin());

        return result;
    }

}
