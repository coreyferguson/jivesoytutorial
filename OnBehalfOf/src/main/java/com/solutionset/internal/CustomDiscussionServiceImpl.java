package com.solutionset.internal;

import com.jivesoftware.base.User;
import com.jivesoftware.base.aaa.AuthenticationProvider;
import com.jivesoftware.community.content.discussion.rest.beans.CreateDiscussionServiceBean;
import com.jivesoftware.community.content.discussion.rest.beans.UpdateDiscussionServiceBean;
import com.jivesoftware.community.content.discussion.rest.impl.CreateDiscussionStrategy;
import com.jivesoftware.community.content.discussion.rest.impl.UpdateDiscussionStrategy;
import com.jivesoftware.community.content.publish.context.PublishContext;
import com.jivesoftware.community.content.publish.context.PublishContextFactory;
import com.jivesoftware.community.content.publish.context.impl.PublishContextOption;
import com.jivesoftware.community.content.rest.ContentServiceHelper;
import org.apache.cxf.jaxrs.ext.MessageContext;
import org.apache.cxf.jaxrs.ext.multipart.MultipartBody;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

/**
 * TODO: Javadoc
 */
@Service("customDiscussionServiceImpl")
public class CustomDiscussionServiceImpl implements CustomDiscussionService {

    @Autowired
    private AuthenticationProvider authenticationProvider;

    @Autowired
    private CustomCreateDiscussionStrategy createDiscussionStrategy;

    @Autowired
    private UpdateDiscussionStrategy updateDiscussionStrategy;

    @Autowired
    private PublishContextFactory publishContextFactory;

    @Autowired
    private ContentServiceHelper contentServiceHelper;

    @Context
    private MessageContext messageContext;

    @Override
    public Response create(CustomCreateDiscussionServiceBean bean) {
        validateEmptyFields(bean);

        PublishContext context = publishContextFactory.fromBean(bean, messageContext);

        return contentServiceHelper.publishAndRedirect(context, createDiscussionStrategy);
    }

    @Override
    public Response create(MultipartBody body) {
        PublishContextOption contextOption = contentServiceHelper.extractPublishContext(body, new CreateDiscussionServiceBean(), messageContext);

        //to catch any attachment or io exceptions
        if (!contextOption.isValid()) {
            return contextOption.getResponseBuilder().buildHTML();
        }

        PublishContext context = contextOption.getPublishContext();
        validateEmptyFields(context.<CreateDiscussionServiceBean>getBean());

        return contentServiceHelper.publishAndRedirect(context, createDiscussionStrategy);
    }

    @Override
    public Response cancel(UpdateDiscussionServiceBean bean) {
        return contentServiceHelper.cancelAndRedirect(bean);
    }

    @Override
    public Response update(long threadID, UpdateDiscussionServiceBean bean) {
        bean.setThreadID(threadID);

        PublishContext context = publishContextFactory.fromBean(bean, messageContext);
        return contentServiceHelper.publishAndRedirect(context, updateDiscussionStrategy);
    }

    @Override
    public Response update(long threadID, MultipartBody body) {

        PublishContextOption contextOption = contentServiceHelper.extractPublishContext(body, new UpdateDiscussionServiceBean(), messageContext);

        //to catch any attachment or io exceptions
        if (!contextOption.isValid()) {
            return contextOption.getResponseBuilder().buildHTML();
        }

        PublishContext context = contextOption.getPublishContext();

        UpdateDiscussionServiceBean bean = context.getBean();
        bean.setThreadID(threadID);

        return contentServiceHelper.publishAndRedirect(context, updateDiscussionStrategy);
    }

    private void validateEmptyFields(CreateDiscussionServiceBean bean) {
        Assert.hasText(bean.getSubject());
        Assert.hasText(bean.getBody());

        User user = authenticationProvider.getJiveUser();
        if (user.isAnonymous()) {
            Assert.hasText(bean.getName());
            Assert.hasText(bean.getEmail());
        }
    }
}
