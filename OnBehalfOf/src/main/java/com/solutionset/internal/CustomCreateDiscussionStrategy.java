package com.solutionset.internal;

import com.jivesoftware.base.User;
import com.jivesoftware.base.UserManager;
import com.jivesoftware.base.UserNotFoundException;
import com.jivesoftware.base.aaa.AuthenticationProvider;
import com.jivesoftware.base.wiki.WikiContentHelper;
import com.jivesoftware.community.*;
import com.jivesoftware.community.content.action.IngressFilter;
import com.jivesoftware.community.content.publish.PublishResponse;
import com.jivesoftware.community.content.publish.context.PublishContext;
import com.jivesoftware.community.content.publish.impl.AbstractPublishStrategy;
import com.jivesoftware.community.impl.DbForumMessage;
import com.jivesoftware.community.quest.QuestManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;

/**
 * TODO: Javadoc
 */
@Component
public class CustomCreateDiscussionStrategy extends AbstractPublishStrategy<ForumThread>  {

    private Logger log = Logger.getLogger(CustomCreateDiscussionStrategy.class);

    @Autowired
    private ForumManager forumManager;

    @Autowired
    private AuthenticationProvider authenticationProvider;

    @Autowired
    private IngressFilter ingressFilter;

    @Autowired
    private QuestionManager questionManager;

    @Autowired
    private UserManager userManager;

    @Override
    public ForumThread create(PublishContext context, PublishResponse.Builder responseBuilder) {
        CustomCreateDiscussionServiceBean bean = context.getBean();

        User user;
        if (bean.getOnBehalfOf() == null) {
            user = authenticationProvider.getJiveUser();
        } else {
            try {
                user = userManager.getUser(bean.getOnBehalfOf());
            } catch (UserNotFoundException e) {
                log.error("Could not find user being acted on behalf of.", e);
                user = authenticationProvider.getJiveUser();
            }
        }
        JiveContainer container = context.getJiveContainer();

        // TODO - forcibly casting to DbForumMessage in order to set container.  Needs proper API.
        DbForumMessage rootMessage = (DbForumMessage) forumManager.createMessage(container, user);
        rootMessage.setSubject(bean.getSubject());

        //filter and apply render plugins
        Document body = ingressFilter.filterRTE(bean.getBody(), rootMessage);

        //body should not be empty after render plugins are applied
        if (WikiContentHelper.isEmpty(body)) {
            responseBuilder.addFieldError("body", "post.err.pls_enter_a_msg.text");
            return null;
        }

        rootMessage.setBody(body);

        //guest user means we want to set the name and email properties
        if (user.isAnonymous()) {
            rootMessage.getProperties().put(DbForumMessage.PROP_NAME, bean.getName());
            rootMessage.getProperties().put(DbForumMessage.PROP_EMAIL, bean.getEmail());

            // If IP tracking is enabled, get the page user's IP and save it as a message property
            if (JiveGlobals.getJiveBooleanProperty("skin.default.trackIP")) {
                String ipAddress = context.getIpAddress();
                if (ipAddress != null) {
                    rootMessage.getProperties().put(DbForumMessage.PROP_IP, ipAddress);
                }
            }

        }

        // if this is from a quest we need to set the property
        if (bean.getFromQuest() != null && !bean.getFromQuest().isEmpty()) {
            rootMessage.getProperties().put(QuestManager.FROM_QUEST_KEY, bean.getFromQuest());
        }

        // Update the container to what was selected
        rootMessage.setContainerID(container.getID());
        rootMessage.setContainerType(container.getObjectType());

        ForumThread thread = forumManager.createThread(container, rootMessage);
        forumManager.addThread(container, thread);

        //mark as question if desired
        if (bean.isMarkAsQuestion()) {
            questionManager.createQuestion(thread);
        }

        return thread;
    }

}
