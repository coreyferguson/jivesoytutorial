package com.solutionset.internal;

import com.jivesoftware.community.content.discussion.rest.beans.CreateDiscussionServiceBean;

/**
 * TODO: Javadoc
 */
public class CustomCreateDiscussionServiceBean extends CreateDiscussionServiceBean {
    private Long onBehalfOf;

    public Long getOnBehalfOf() {
        return onBehalfOf;
    }

    public void setOnBehalfOf(Long onBehalfOf) {
        this.onBehalfOf = onBehalfOf;
    }
}
