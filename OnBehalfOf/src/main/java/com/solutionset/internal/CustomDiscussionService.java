package com.solutionset.internal;

import com.jivesoftware.community.content.discussion.rest.beans.CreateDiscussionServiceBean;
import com.jivesoftware.community.content.discussion.rest.beans.UpdateDiscussionServiceBean;
import com.jivesoftware.community.content.rest.ContentService;
import org.apache.cxf.jaxrs.ext.multipart.MultipartBody;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * TODO: Javadoc
 */
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public interface CustomDiscussionService extends ContentService<CustomCreateDiscussionServiceBean, UpdateDiscussionServiceBean> {

    @POST
    @Path("/")
    @Override
    Response create(CustomCreateDiscussionServiceBean bean);

    @POST
    @Path("/")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.TEXT_HTML)
    @Override
    Response create(MultipartBody body);

    @POST
    @Path("/cancel")
    @Override
    Response cancel(UpdateDiscussionServiceBean bean);

    @PUT
    @Path("/{threadID}")
    @Override
    Response update(@PathParam("threadID") long threadID, UpdateDiscussionServiceBean bean);

    @POST
    @Path("/{threadID}")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.TEXT_HTML)
    @Override
    Response update(@PathParam("threadID") long threadID, MultipartBody body);
}
